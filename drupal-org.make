core = 8.x
api = 2

projects[drupal][version] = 8.0.5

defaults[projects][subdir] = "contrib"

projects[video_embed_field][type] = module
projects[views_infinite_scroll][type] = module

projects[image_style_quality][type] = module
projects[responsive_image_automatic][type] = module
projects[reference_table_formatter][type] = module
projects[colorbox][type] = module
projects[admin_toolbar][type] = module

projects[file_entity][type] = module
projects[file_entity][download][type] = git
projects[file_entity][download][url] = https://git.drupal.org/project/file_entity.git
projects[file_entity][download][branch] = 8.x-2.x
projects[file_entity][download][revision] = af4131334c88a7e7bd045874070c79b9af7d842d

projects[default_content][type] = module
projects[default_content][download][type] = git
projects[default_content][download][url] = https://git.drupal.org/project/default_content.git
projects[default_content][download][branch] = 8.x-1.x
projects[default_content][download][revision] = ae81658bb2955c47c5ece5f9e52eea2e956ad74d

projects[title][type] = module
projects[title][download][type] = git
projects[title][download][url] = https://git.drupal.org/project/title.git
projects[title][download][branch] = 8.x-2.x
projects[title][download][revision] = b163e2d50b3add3842ac67ea11cced5eb5d7ceaf

projects[fences][type] = module
projects[fences][download][type] = git
projects[fences][download][url] = https://git.drupal.org/project/fences.git
projects[fences][download][branch] = 8.x-2.x
projects[fences][download][revision] = e034397383724236453342cbc0a498193b607c00

projects[video_portal][type] = profile
projects[video_portal][subdir] = ""
projects[video_portal][download][type] = git
projects[video_portal][download][url] = sam@git.drupal.org:sandbox/sam/2609842.git
projects[video_portal][download][branch] = 8.x-1.x
projects[video_portal][download][revision] = c82edc1c640a28de7f870bf5e17eeec560749062
